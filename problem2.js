const fs = require('fs');
const path = require('path');

function readFile(filename, uppercaseAndMakeFile, readnewfile, readSentenceAndSort, deleteAllInfilenames) {
    let fullpath = path.join(__dirname, filename)
    fs.readFile(fullpath, 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            //console.log(data);
            uppercaseAndMakeFile(data, readnewfile, readSentenceAndSort, deleteAllInfilenames);
        }
    })

}

function uppercaseAndMakeFile(data, readnewfile, readSentenceAndSort, deleteAllInfilenames) {
    let upperData = data.toUpperCase();
    let fullpathupper = path.join(__dirname, 'upper.txt');
    fs.writeFile(fullpathupper, upperData, (err) => {
        if (err)
            console.log(err);
        else {
            console.log("data written in upper.txt");
        }
    })
    let fullpathfile = path.join(__dirname, 'filenames.txt');
    fs.writeFile(fullpathfile, "upper.txt" + "\n", (err) => {
        if (err)
            console.log(err);
        else {
            console.log("filename upper.txt has written in filenames");
        }
    })
    readnewfile(readSentenceAndSort, deleteAllInfilenames);

}

function readnewfile(readSentenceAndSort, deleteAllInfilenames) {
    let fullpath = path.join(__dirname, "upper.txt");
    fs.readFile(fullpath, 'utf-8', (err, data) => {
        if (err) {
            console.log(err);
        } else {
            let lowerData = data.toLowerCase();
            lowerData = lowerData.trim();
            let splittedstring = ((lowerData.replace("\n", ""))).split(".");
            splittedstring = splittedstring.map((sentence) => sentence.trim("\n") + ".");
            //console.log(splittedstring);
            let backtostr = splittedstring.join("\n");
            //console.log(backtostr);
            let filepath = path.join(__dirname, 'sentence.txt')
            fs.writeFile(filepath, backtostr, (err) => {
                if (err)
                    console.log(err);
                else {
                    console.log("data written in sentence.txt")
                }
            });
            setTimeout(() => {
                let fullpath = path.join(__dirname, 'filenames.txt')
                fs.appendFile(fullpath, 'sentence.txt\n', (err) => {
                    if (err)
                        console.log(err);
                    else {
                        console.log("sentence name has written into filenames.txt");
                    }
                });
            }, 2000);
            readSentenceAndSort(deleteAllInfilenames);
        }
    })
}

function readSentenceAndSort(deleteAllInfilenames) {
    setTimeout(() => {
        let fullpath = path.join(__dirname, 'sentence.txt')
        fs.readFile(fullpath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                // console.log(data);
                let sorteddata = data.split("\n").sort();
                sorteddata = sorteddata.join("\n");
                //console.log(sorteddata);
                let filepath = path.join(__dirname, 'sortedData.txt')
                fs.writeFile("../sortedData.txt", sorteddata, (err) => {
                    if (err)
                        console.log(err);
                    else {
                        console.log("data written in sortedData.txt")
                    }
                });
                fullpath = path.join(__dirname, 'filenames.txt')
                setTimeout(() => {
                    fs.appendFile(fullpath, "sortedData.txt\n", (err) => {
                        if (err)
                            console.log(err);
                        else {
                            console.log("filename sorteddata written in filenames.txt")
                        }
                    });
                }, 2000);
                deleteAllInfilenames()
            }
        })
    }, 3000);
}

function deleteAllInfilenames() {
    setTimeout(() => {
        let fullpath = path.join(__dirname, 'filenames.txt');
        fs.readFile(fullpath, 'utf-8', (err, data) => {
            if (err) {
                console.log(err);
            } else {
                //console.log(data);
                data = data.trim();
                let delfiles = data.split("\n");
                console.log(delfiles)
                delfiles.map((eachFile) => {
                    let filepath = path.join(__dirname, eachFile);
                    fs.unlink(filepath, (err) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log("file deleted");
                        }
                    })

                })
            }
        })
    }, 4000);

}

module.exports = { readFile, uppercaseAndMakeFile, readnewfile, readSentenceAndSort, deleteAllInfilenames };