const fs = require('fs');
const path = require('path');

function toCreateAndDeleteFiles(dirname,numberOfFiles,createdirectory,creatingfiles,deleteallfiles){
//create dir
createdirectory(dirname,numberOfFiles,creatingfiles,deleteallfiles);

}


function createdirectory(dirname,numberOfFiles,creatingfiles,deleteallfiles) {
    let dirpath=path.join(__dirname,dirname)
    fs.mkdir(dirpath, (err) => {
        if (err) {
            console.log(err);
        } else {
            console.log("directory is created");
        }
    })
    creatingfiles(dirname,numberOfFiles,deleteallfiles);
}

function creatingfiles(dirname, numberOfFiles,deleteallfiles) {
    setTimeout(()=>{
    let number = 1;
    do {
        let pathtodir=dirpath=path.join(__dirname,dirname)
        let fullpath = pathtodir+ "/" + "JSONfile_" + number + ".json"
        fs.writeFile(fullpath,
            `file 1${number} is created successfully`,
            (err) => {
                console.log("file is created");
            })
        number++;
    } while (number < numberOfFiles + 1)
    },2000);
    deleteallfiles(dirname);
}

function deleteallfiles(dirname) {
    setTimeout(() => {
        let dirfullpath = path.join(__dirname,dirname);

        //console.log(dirfullpath);
        fs.readdir(dirfullpath,(err,files)=>{
            if (err){
                console.log(err);
            }else{
                console.log(files);
                files.map((file)=>{
                    let filepath=path.join(dirfullpath,file);
                    fs.unlink(filepath,(err)=>{
                        if(err){
                            console.log(err);
                        }else{
                            console.log("file deleted");
                        }
                    })
                })

            }
        });
    }, 3000);
}

module.exports = {
    toCreateAndDeleteFiles,
    createdirectory,
    creatingfiles,
    deleteallfiles
};


