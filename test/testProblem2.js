const { readFile, uppercaseAndMakeFile, readnewfile, readSentenceAndSort, deleteAllInfilenames } = require('../problem2')
const filename = 'lipsum.txt'
readFile(filename, uppercaseAndMakeFile, readnewfile, readSentenceAndSort, deleteAllInfilenames);